# PhishWarn

This is the official demo page of PhishWarn. You can find its main repository at [PhishWarn/PhishWarn](https://codeberg.org/PhishWarn/PhishWarn#phishwarn).

## Licence

PhishWarn is licensed under the [For Good Eyes Only Licence v0.2](https://codeberg.org/PhishWarn/pages/src/branch/master/For%20Good%20Eyes%20Only%20Licence%20v0.2.pdf). Do not re-use PhishWarn's title and logo/icon.

All versions of PhishWarn are licensed under v0.2, which replaces v0.1 under which PhishWarn was previously licensed. v0.1 is therefore void for any versions of PhishWarn.

PhishWarn uses [Font Awesome](https://fontawesome.com) ([Font Awesome Free Licence](https://github.com/FortAwesome/Font-Awesome/blob/6.x/LICENSE.txt)) and [Bootstrap Icons](https://icons.getbootstrap.com/) ([MIT Licence](https://github.com/twbs/icons/blob/main/LICENSE.md)).

![](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/branch/master/banner/Banner%20250.png)