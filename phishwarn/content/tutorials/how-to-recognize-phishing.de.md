---
title: Wie man Phishing erkennt
description: Efahre mehr darüber, wie man Phishing erkennt.
layout: tutorials

links:
  - bsi.de
  - mobilsicher.de
  - kuketz.de
---
{{< contentHeadline >}}Woran erkennt man Phishing?{{< /contentHeadline>}}
{{< article >}}
Phishing zu erkennen, kann ziemlich schwer sein. Hier sind einige Anzeichen, die einem sagen können, ob eine Nachricht verdächtig ist.

{{< subHeadline "top" >}}Unprofessionelle Adresse{{< /subHeadline >}}

Professionelle Unternehmen nutzen ihre eigenen Domains für ihre E-Mail-Adressen statt kostenloser E-Mail-Anbieter, z.B.: support@twitter.com statt twitter@yahoo.com.

{{< details >}}
Professionelle Unternehmen haben fast immer eine Website mit eigener Domain, z.B. apple.com für den Smartphone-Hersteller Apple oder nytimes.com für die New York Times. Wann immer du eine E-Mail von einem Unternehmen erhältst, solltest du im Internet nach dessen echter Website-Domain suchen und überprüfen, ob sie der E-Mail-Domain entspricht.
<br><br>
Sagen wir, du hast eine E-Mail scheinbar von Twitter bekommen. Falls du es nicht sowieso weißt, findest du schnell heraus, dass deren Domain {{< important >}}twitter.com{{< /important >}} ist. Was ist nun die E-Mail-Adresse des Absenders? Sagen wir, sie ist {{< important >}}support@twitter.com{{< /important >}}. Die Domain ist der Teil hinter dem @-Zeichen. In diesem Fall ist es also {{< important >}}twitter.com{{< /important >}}. Das wirkt definitiv legitim.
<br><br>
Aber was, wenn der Sender {{< important >}}twitter@mail.com{{< /important >}} ist? Auch in der Adresse findet sich das Wort „Twitter“. Der Unterschied ist: Es handelt sich bloß um einen beliebigen Nutzernamen, der bei jeder E-Mail-Domain verwendet werden könnte. Die Domain ist {{< important >}}mail.com{{< /important >}}, ein beliebter E-Mail-Anbieter für Privatnutzer. Ein Unternehmen wie Twitter würde niemals solch einen unprofessionellen Mail-Dienst nutzen, wenn es auch auf seine schöne twitter.com-Domain zurückgreifen könnte.
{{< /details >}}

{{< subHeadline >}}Fake-Support kostenloser E-Mail-Dienste{{< /subHeadline >}}

Einige Betrüger versuchen nicht, ein Unternehmen zu imitieren, indem sie einen kostenlosen Dienst wie mit dem obigen Beispiel twitter@yahoo.com verwenden. Stattdessen versuchen sie, den kostenlosen E-Mail-Dienst {{< important >}}an sich{{< /important >}} zu imitieren, z.B.: {{< important >}}freundlicher-kundendienst@yahoo.com{{< /important >}}. Die scheint Yahoos echter Support zu sein, doch in Wahrheit ist es nur irgendein Nutzerkonto, welches eben einen „merkwürdigen“ Benutzernamen hat. Daher solltest du immer googlen, ob die E-Mail-Adresse des Absenders tatsächlich zum Support deines E-Mail-Anbieters gehört.

{{< subHeadline >}}Falsche Account-Adresse{{< /subHeadline >}}

Ein Betrüger weiß für gewöhnlich nicht, welche E-Mail-Adresse zu welchem Online-Account gehört (oder andersherum), sodass du etwa eine Phishing-E-Mail scheinbar von Amazon an eine deiner E-Mail-Adressen bekommst, die gar nicht zu deinem Amazon-Account gehört. Und da Amazon niemals E-Mails an Adressen schicken würde, die nicht zum jeweiligen Nutzerkonto gehören, kannst du bei Mails an die falsche Adresse ziemlich sicher sein, dass es Phishing-Versuche sind.

{{< subHeadline >}}Nachrichten von Diensten, die du gar nicht benutzt{{< /subHeadline >}}

Betrüger imitieren einfach jedes beliebte Unternehmen (bzw. Dienst), das es gibt, weil dann die Chancen hoch sind, dass das zufällig ausgewählte Opfer tatsächlich den jeweiligen Dienst nutzt. Aber sagen wir, du erhältst eine E-Mail von TikTok, aber du nutzt gar nicht TikTok – dann handelt es sich offensichtlich um eine Fälschung.

{{< subHeadline >}}Subdomain statt Domain{{< /subHeadline >}}

Manche Betrüger kaufen eigene Domains für ihre E-Mail-Adressen und passen diese so an, dass sie legitim aussehen, z.B.: {{< important >}}support@amazon.de.example.com{{< /important >}}.
Es sieht (zumindest auf den ersten Blick) so aus, als sei amazon.com die Domain, aber in Wahrheit ist es example.com.

{{< details >}}
Viele Betrüger belassen es nicht bei solch einfachen Dingen wie twitter@mail.com zu nutzen. Stattdessen kaufen sie eigene Domains und tarnen sie auf eine Weise, dass sie wie legitime Adressen aussehen. Eine verbreitete Methode ist es, eine Subdomain vor die tatsächliche Domain zu setzen.
<br><br>
Jede Internet-Adresse besteht aus verschiedenen Teilen. Grundsätzlich handelt es sich um dieses Schema:
<br><br>
{{< xScroll >}}Protokoll://Subdomain.Domain.TopLevelDomain/Pfad/zum/Ordner/Dateiname.Dateityp{{< /xScroll >}}

Beispiel: https://pixelcode.codeberg.page/phishwarn/index.html
<br><br>
Für E-Mails ist nur dieser Teil relevant:
{{< important >}}Subdomain.Domain.TLD{{< /important >}}
<br><br>
Das Wichtige daran ist, dass jede Domain.TLD einzigartig ist. Es gibt nur ein ebay.com und nur ein freedom.press.
<br><br>
Auf der anderen Seite {{< important >}}sind Subdomains NICHT einzigartig{{< /important >}}. Jeder Domainbesitzer kann jede beliebige Subdomain und beliebig viele Subdomains festlegen. Daher wäre eine E-Mail-Domain wie diese möglich: {{< important >}}amazon.de.example.com{{< /important >}}.
<br><br>
Beachte, dass die tatsächliche Domain NICHT amazon.de ist, sondern {{< important >}}example.com{{< /important >}}. Viele Nutzer würden jedoch nicht erkennen, dass amazon.de nicht die Domain ist, sondern denken, dass der Absender tatsächlich Amazon ist, wo doch amazon.de in Wirklichkeit nur eine Kombination aus zwei Subdomains ist: amazon und de.
<br><br>
Die echte Domain ist immer die letzte Zeichenkette vor der Top-Level-Domain. Die TLD ist so etwas wie .com, .net, .gov.uk, .de, .fr usw. (manchmal kann es auch etwas Ausgefallenes wie .page, .social oder .dev sein).
<br><br>
Wenn du dir bei diesem Domain-Zeugs nicht sicher bist, merke dir bloß, dass viele Punkte innerhalb einer E-Mail-Domain verdächtig sind (also viele Subdomains).
{{< /details >}}

{{< figure "phishing-explanation/dhl-phishing-explanation.png" "Vage Anrede: „Hallo“; viele Grammatikfehler (z.B. klein geschriebene Anrede „Sie“); kein Absender; keine Referenznummer; vage Begründung">}}
Eine angeblich von DHL stammende Phishing-E-Mail, die bei genauerem Hinsehen als sehr verdächtig erkannt werden kann. Gut zu wissen: DHL {{< link "https://www.dhl.com/de-de/home/fusszeile/betrugserkennung.html">}}bittet{{< /link >}} seine Kunden, Phishing-Versuche an {{< link "mailto:phishing-dpdhl@dhl.com">}}phishing-dpdhl@dhl.com{{< /link >}} zu melden.
{{< /figure >}}

{{< subHeadline >}}Hoher Druck auf dich{{< /subHeadline >}}

Betrüger üben hohen Druck auf dich aus, indem sie Dringlichkeit nutzen, schwerwiegende Konsequenzen androhen oder vorgeben, wichtige Autoritäten zu sein.

{{< details >}}
Wie oben erklärt, versuchen Betrüger dich panisch werden zu lassen, indem sie hohen Druck auf dich ausüben.
So machen sie es:
<br><br>
{{< important >}}1. Dringlichkeit:{{< /important >}} Wenn du angewiesen wirst, eine bestimmte Aktion innerhalb von
etwa 24 oder 48 Stunden auszuführen, solltest du wohl einen Augenblick innehalten. Frag dich selbst: Wirkt die Sache wirklich so dringend? Würde der echte Sender solch eine wichtige Mitteilung via E-Mail/SMS etc. schicken? Warum würde er nicht einfach einen Brief schicken, um den Empfang sicherzustellen?
<br><br>
{{< important >}}2. Schwerwiegende Konsequenzen:{{< /important >}}> Wenn dir mit schwerwiegenden Konsequenzen gedroht wird, wie einer Kontosperrung oder verklagt, verhaftet oder gefeuert zu werden, ist es die gleiche Situation: Würde der echte Sender dir so drohen – insbesondere via E-Mail/SMS etc.?
<br><br>
{{< important >}}3. Hohe Autorität:{{< /important >}}Viele Betrüger geben vor, jemand wie dein Arzt, die Polizei,
generell die Regierung oder sonst eine hohe Autorität zu sein. Halte für einen Moment inne und denk nach, ob du jemals eine Nachricht des jeweiligen Absenders über diesen Kommunikationsweg erhalten hast. Falls nicht: Bist du dir sicher, dass <i>die Polizei</i> dir eine E-Mail senden würde? Kennt man dort überhaupt deine Adresse? Warum würden sie solch einen unsicheren Kommunikationsweg wählen, wenn sie dir auch einen Brief schicken könnten?
<br><br>
Zumindest in der westlichen Welt würde die Polizei niemals E-Mails oder Chatnachrichten versenden, sondern stattdessen Briefe. In dringenden Fällen würden sie Polizeibeamte entsenden, um mit dir persönlich zu sprechen.
{{< /details >}}

{{< subHeadline >}}Vage Anrede{{< /subHeadline >}}

Viele Betrüger nutzen vage Anredeformen statt deines Namens, weil sie ihn einfach nicht kennen. Zum Beispiel würde eine Bank niemals etwas wie "Geschätzter Kunde..." oder "Sehr geehrte Damen und Herren..." als Anrede in einer Nachricht nutzen, die an einen bestimmten Kunden adressiert ist. Wenn der Sender also deinen Namen nicht erwähnt, ihn aber kennen sollte, falls er der wäre, für den er sich ausgibt, dann solltest du auf der Hut sein.

{{< subHeadline >}}Schlechte Grammatik und Rechtschreibung{{< /subHeadline >}}

Manche Phishing-Nachrichten enthalten viele und schwere Grammatik- und Rechtschreibfehler – anders als legitime Nachrichten von professionellen Unternehmen, die sicherstellen, dass sie ihre Reputation nicht beschädigen.

{{< details >}}
Legitime Absender, vor allem große Unternehmen, würden sicherlich genau darauf Acht geben, ihren Ruf nicht durch schlechte Grammatik
und Rechtschreibung zu beschädigen. Hier sind einige Gründe, warum Phishing-Texte sprachlich fehlerhaft sein können:
<br><br>
{{< important >}}1. Schlechte Online-Übersetzung:{{< /important >}} Viele Betrüger nutzen Online-Übersetzer niedriger Qualität, um ihre Nachrichten in verschiedenen Sprachen zu verbreiten. Die schlechte Qualität von Google Translate ist sogar zu einem Internet-Meme geworden.
<br><br>
{{< important >}}2. Kein Muttersprachler:{{< /important >}} Viele Betrüger kommen einfach aus Ländern, in denen die Muttersprache des Opfers nicht weit verbreitet ist. Viele von ihnen kommen auch aus armen Regionen der Welt, wo es grundsätzlich kein gutes Bildungssystem gibt.
<br><br>
{{< important >}}3. Effizienz:{{< /important >}} Es kann auch einfach effizienter sein, Tausende Phishing-Mails ohne angemessene Grammatik zu versenden, statt einige wenige qualitativ hochwertige Mails herzustellen.
<br><br>
{{< important >}}4. Schlechte Bildung:{{< /important >}} Menschen, die phishen müssen, um Geld zu verdienen, können auch einfach eine schlechte Bildung haben, so wie es bei vielen normalen Kriminellen der Fall ist.
<br><br>
Wie dem auch sei: Beachte, dass es in letzter Zeit auch einige Phishing-Nachrichten mit guter Grammatik gegeben hat. Verlasse dich also nicht auf schlechte Sprache als Warnsignal.
{{< /details >}}

{{< subHeadline >}}Getarnte Links{{< /subHeadline >}}

Oftmals tarnen Betrüer ihre Phishing-Links, indem sie unverdächtige Anzeigetexte wählen, z.B.
„www.wikipedia.org“ als Anzeigetext für example.com als tatsächlichen Link: {{< link "https://example.com">}}www.wikipedia.org{{< /link >}}

{{< details >}}
Dies ist möglich, da die „Internet-Sprache“ HTML es vorsieht, beliebige Zeichenketten mit einem Link zu versehen, also beispielsweise: {{< link "https://codeberg.org/PhishWarn/PhishWarn">}}Quellcode{{< /link >}}. In diesem Falle ist {{< important >}}codeberg.org/PhishWarn/PhishWarn{{< /important >}} der unsichtbare Link und „Quellcode“ der sichtbare Anzeigetext.
<br><br>
Betrüger wählen als sichtbaren Anzeigetext also z.B. {{< important >}}„www.amazon.de/gp/customer-service“{{< /important >}}, verlinken aber stattdessen auf eine Phishing-Seite, z.B. example.com (diese hier ist natürlich ungefährlich). Das sieht dann so aus: {{< link "https://example.com">}}www.amazon.de/gp/customer-service{{< /link >}}. Aber wie erkennt man so etwas? Praktisch alle Browser zeigen die Link-URL am unteren Bildschirmrand an, wenn man den Mauszeiger über den Link bewegt. Probier’s aus: {{< link "https://twitter.com/snowden">}}Beispiel-Link{{< /link >}}
<br><br>
Konzentriere dich ausschließlich auf die URL-Vorschau (nicht auf den möglicherweise gefälschten Anzeigetext) und schätze ein, ob die URL vertrauenswürdig ist oder nicht. Wenn du dir nicht sicher bist, klicke NICHT auf den Link.
{{< /details >}}

{{< figure "phishing-explanation/dkb-phishing-explanation.png" "Vage Anrede: „Sehr geehrter Kunde“; gebrochenes Deutsch (z.B. ist statt „Link“ die Rede von „Verbindung“); ungenaue Absender-Angabe („Gebührenfrei Kundenbetreuung“ [sic!]); merkwürdige Begründung">}}
Eine angeblich von der DKB stammende Phishing-E-Mail, die den schädlichen Phishing-Link gut tarnt, indem die vermeintliche URL "www.dkb.de/banking" bloß als Anzeigetext verwendet wird.
Gut zu wissen: Die DKB <a class="link" href="https://www.dkb.de/sicherheit/phishing">bittet</a> ihre Kunden, Phishing-Versuche an
{{< link "mailto:phishingverdacht@dkb.de">}}phishingverdacht@dkb.de{{< /link >}} zu melden.
{{< /figure >}}

{{< subHeadline >}}Fremdsprache{{< /subHeadline >}}

Einige Betrüger kennen die Muttersprache des Opfers nicht oder kümmern sich einfach nicht darum, sodass etwa ein Franzose eine deutsche Phishing-Mail von seiner französischen Bank erhalten könnte. Warum würde die Bank ihre Nachricht in eine andere Sprache übersetzen, die ihr Kunde wahrscheinlich ohnehin nicht einmal spricht? Das würde keinen Sinn ergeben.

{{< subHeadline >}}Abfrage sensibler Nutzerdaten{{< /subHeadline >}}

Das Ziel der Betrüger ist es, an die Login-Daten oder andere persönliche Information ihrer Opfer zu gelangen, weshalb sie offen nach Passwörtern, PINs oder TAN-Codes fragen. Legitime Unternehmen dagegen würden niemals nach sensiblen Nutzerdaten in einer E-Mail oder Chatnachricht fragen, weil das höchst unsicher wäre. Zudem benötigen Supportmitarbeiter NICHT das Passwort des Nutzers, weil sie ohnehin vollen Zugang auf die Nutzerdatenbank des Unternehmens haben.

{{< subHeadline >}}Anhänge und Handlungsaufforderungen{{< /subHeadline >}}

Alle Phishing-Nachrichten zeichnen sich dadurch aus, dass sie das Opfer zu einer bestimmten Handlung auffordern, also z.B. auf einen Link zu klicken oder den großen leuchtenden Button zu betätigen. Nicht wenige Betrugs-Mails enthalten zudem auch Anhänge schädlichen Inhalts (dazu unten mehr). Zwar weisen auch echte Mails den Nutzer zu bestimmten Dingen an, doch sie sind meist weitaus weniger aggressiv bzw. fordernd und sind auch nicht zwangsläufig auf Links und Buttons beschränkt. Dass der echte Support einen Anhang mitsendet, ist dagegen eher selten.

{{< subHeadline >}}Ausführbare Dateien{{< /subHeadline >}}

Manche Betrüger versuchen auch, das Opfer dazu zu bringen, ein Programm oder eine App herunterzuladen, indem sie es anweisen, eine beigefügte .exe-, .msi-, .app oder .apk-Datei etc. zu installieren. Oft sind solche Dateien zudem als harmlos getarnt (z.B. {{< important >}}„versandbestätigung.pdf.exe“{{< /important >}}) oder in einer ZIP-Datei vor Spam-Filtern versteckt.

{{< details >}}
Legitime Absender würden so etwas nie tun, sondern
auf eine legitime Website verweisen, wo man das Programm herunterladen kann, falls überhaupt benötigt. Du solltest niemals irgendetwas
herunterladen oder installieren, das in einer verdächtigen E-Mail genannt ist.
<br><br>
{{< important >}}Achtung! Viele ausführbare Dateien sind nicht als solche erkennbar!{{< /important >}}
<br>So wie Phishing-Websites mithilfe von legitim wirkenden Subdomains getarnt werden, versuchen Betrüger, den Dateityp beigefügter ausführbarer Dateien zu verschleiern, indem sie sie beispielsweise {{< important >}}„rechnung.pdf.exe“{{< /important >}} oder {{< important >}}„urlaubsfoto.jpg.app“{{< /important >}} nennen. In diesem Fall ist die tatsächliche Dateiendung {{< important >}}.exe{{< /important >}} bzw. {{< important >}}.app{{< /important >}} und NICHT PDF bzw. JPG.
<br><br>
Zudem zeigen viele E-Mail-Programme den Dateityp standardmäßig nicht an, weshalb statt „rechnung.pdf.exe“ nur „rechnung.pdf“ angezeigt würde – was fatalerweise alles andere als verdächtig wirkt!
<br><br>
Außerdem verstecken einige Betrüger ihre ausführbaren Dateien in .zip-Archiven, um zu vermeiden, dass etwa ein Spam-Filter den verdächtigen .exe-Anhang erkennt. Wenn du also eine ZIP-Datei im Anhang siehst, solltest du besondere Vorsicht walten lassen!
{{< /details >}}
{{< /article >}}

{{< teaser "preventPhishing" "de" >}}
