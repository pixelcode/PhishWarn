#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

yaml() {
  python3 -c "import yaml;print(yaml.safe_load(open('$1'))$2)"
}

commit() {
  echo ""
  git add .
  git commit -m "$commitMessage"
}

push() {
  echo ""
  echo -n "Push branch master to origin? [y/n]: "
  read push

  if [[ $push == "y" ]]; then
    echo ""
    git push origin master
  fi

  printf "\n"
}

hugoRepo=$PWD
publishDir=$(yaml config.yaml "['publishDir']")
commitMessage=""

echo ""
printf "\e[32m\e[1mgit status\e[0m\n\n"
cd ..
git status

echo ""
echo -n "Commit changes in Hugo repository? [y/n]: "
read commitHugo

if [[ $commitHugo == "y" ]]; then
  echo -n "Commit message: "
  read cm
  commitMessage=$cm
  (commit)
fi

push

printf "\nSwitching to publishDir: \e[32m\e[1m$publishDir\e[0m\n"
cd "$hugoRepo"
cd "$publishDir"
echo ""

printf "\e[32m\e[1mgit status\e[0m\n\n"
git status
echo ""

echo -n "Commit changes in publishDir? [y/n]: "
read commitPublishDir

if [[ $commitPublishDir == "y" ]]; then
  (commit)
fi

push

printf "\nSwitching back to Hugo repo: \e[32m\e[1m$hugoRepo\e[0m\n\n"
cd "$hugoRepo"