---
title: Wie verhindert man, gephisht zu werden?
description: Wie verhindert man, Opfer eines Phishing-Versuchs zu werden?
layout: tutorials

links:
  - bsi.de
  - ncsc.en
  - vbz.de
---
{{< contentHeadline >}}Wie verhindert man, gephisht zu werden?{{< /contentHeadline >}}
{{<article>}}
Logischerweise ist es unmöglich, Phishing-Versuche allgemein zu verhindern. Es gibt jedoch einige Online-Sicherheitsmaßnahmen, die dir dabei helfen können, Betrug zu meiden.

{{< subHeadline "top" >}}Verwende Adblocker{{< /subHeadline >}}

Adblocker sind Anwendungen, die verhinden, dass Online-Werbeanzeigen in Websites oder Anzeigen geladen werden. Viele unterstützen jedoch auch Filterlisten mit schädlichen Websites, um zu verhindern, dass du versehentlich welche davon besuchst. Wirklich gute Adblocker sind {{< link "https://github.com/gorhill/uBlock#ublock-origin">}}uBlock Origin{{< /link >}} (Browser-Addon) und {{< link "https://blokada.org">}}Blokada{{< /link >}} (Android- und iOS-App).

{{< subHeadline >}}Frage den Absender separat, ob die Nachricht wirklich von ihm stammt{{< /subHeadline >}}

Dieser „Trick“ ist so einfach wie effektiv: Frag einfach den vermeintlichen Absender über einen separaten Kommunikationsweg, ob er wirklich die Nachricht gesendet hat. Komische SMS von deinem Freund bekommen? Schreib ihm eine E-Mail. Verdächtige Mail von deiner Bank erhalten? Such deren Telefonnummer auf ihrer echten Website und ruf einfach an. Mit Festnahme durch die Polizei bedroht worden? Geh zur nächsten Polizeidienststelle und frag, was los ist. (Praktischerweise kannst du dort auch gleich die Amtsanmaßung anzeigen.)

{{< subHeadline >}}Halte deine E-Mail-Adresse und Telefonnummer privat{{< /subHeadline >}}

Betrüger können dir nur dann Phishing-Texte schicken, wenn sie deine Kontaktinformationen haben. Halte sie privat und nutze Zweit-Adressen oder -Telefonnummern für Online-Registrierungen etc.

{{< details >}}
Meist bekommen Betrüger deine E-Mail-Adresse oder Telefonnummer über Newsletter-Listen, Online-Shop-Accounts oder Internet-Gewinnspiele. Daher solltest du deine Kontaktinformationen nur Leuten geben, die du persönlich kennst.
<br><br>
Nutze spezielle Zweit-Adressen für Online-Registrierungen, sodass du schon vorher weißt, dass sich im jeweiligen Postfach Phishing und Spam befinden könnte. Probier {{< link "https://relay.firefox.com">}}Firefox Relay{{< /link >}} aus, um deine E-Mail-Adressen privat zu halten.
<br><br>
Ziehe auch den Kauf einer billigen Zweit-SIM-Karte mit separater Nummer in Erwägung, um dich beispielsweise bei Messengern wie Signal zu registrieren, die eine Telefonnummer voraussetzen.
{{< /details >}}

{{< subHeadline >}}Halte deine Software auf dem neuesten Stand{{< /subHeadline >}}

Hacker und Betrüger nutzen oftmals veraltete Software aus: Zum Beispiel missbrauchen sie Sicherheitslücken, um auf fremde Computer zuzugreifen oder sie nutzen die Tatsache aus, dass Filterlisten schädlicher Websites ggf. nicht aktualisiert und gepflegt werden. Darum solltest du automatische Updates überall aktivieren, wo dies möglich ist, um Sicherheits- und Fehlerbehebungen sowie die neuesten Filterlisten zu erhalten.

{{< subHeadline >}}Deaktiviere Punycode{{< /subHeadline >}}

Viele Betrüger registrieren eigene legitim aussehende Domains, in denen sie z.B. das lateinische a durch das zum Verwechseln ähnlich sehende kyrillische а ersetzen. Deaktiviere die Anzeige solcher Sonderzeichen im Browser (siehe {{< link "https://www.heise.de/security/meldung/Browser-noch-immer-fuer-Phishing-per-Unicode-Domain-anfaellig-3686474.html">}}Heise-Anleitung{{< /link >}}) oder nutze das Browser-Addon {{< link "https://addons.mozilla.org/firefox/addon/punycode-domain-detection/">}}PunyCode Domain Detection{{< /link >}}, um dich vor Domains mit Sonderzeichen warnen zu lassen.

{{< details >}}
Gerade westliche Nutzer kennen das Internet fast ausschließlich mit lateinischen Zeichen und haben von kyrillischer oder arabischer Schrift nur in der Schule gehört. Um auch z.B. ukrainische oder taiwanesische URLs korrekt darzustellen, verwenden moderne Browser die Technik "Punycode", die aber auch Betrügern die Möglichkeit gibt, ihre schädlichen Domains mit lateinisch aussehenden Sonderzeichen zu tarnen.
<br><br>
Um zu vermeiden, in Zukunft auf echte Betrügereien hereinzufallen, solltest du entweder Punycode im Browser komplett deaktivieren oder das Addon {{< link "https://addons.mozilla.org/firefox/addon/punycode-domain-detection/">}}PunyCode Domain Detection{{< /link >}} installieren, welches vor URLs mit Punycode warnt.
<br><br>
In Firefox kannst du Punycode deaktivieren, indem du about:config in die Adressleiste eingibst (funktioniert auch auf dem Handy) und dort nach der Einstellung „network.IDN_show_punycode“ suchst, welche du auf „true“ stellst.
{{< /details >}}

{{< figure "tutorials/apple.com.png" "Bildschirmfoto einer Website, deren Adresse wie apple.com aussieht.">}} Diese Punycode-{{< link "https://www.аррӏе.com/">}}Demonstrationswebsite{{< /link >}} scheint die Adresse {{< important >}}apple.com{{< /important >}} zu haben, doch in Wirklichkeit besteht diese komplett aus kyrillischen Buchstaben: аррӏе.
{{< /figure >}}

{{< figure "tutorials/xn--80ak6aa92e.com.png" "Bildschirmfoto derselben Website, diesmal klar als nicht apple.com zu erkennen.">}}
Deaktiviert man Punycode im Browser, wird die Adresse als <a class="important">xn--80ak6aa92e.com</a> angezeigt – definitiv nicht apple.com.
{{< /figure >}}

{{< figure "tutorials/apple.com addon.png" "Das Anti-Punycode-Addon warnt vor einem „possible phishing attempt: the address bar shows apple.com but the real domain name is xn--80ak6aa92e.com“">}}
Das Anti-Punycode-Addon warnt vor einem möglichen Phishing-Versuch.
{{< /figure >}}

{{< subHeadline >}}Verrate nicht zu viel über dich in den sozialen Medien{{< /subHeadline >}}

Betrüger nutzen Open-Source-Ermittlungsmethoden, um mehr über potenzielle Opfer zu erfahren, beispielsweise indem sie ihre Social-Media-Profile überprüfen. Deshalb solltest du immer darüber nachdenken, was du online postest und ob es von Betrügern missbraucht werden könnte. Sieh nach, ob du die Sichtbarkeit deiner Posts auf Freunde und Familie beschränken kannst. Versuche deinen gesamten Account zu sperren, damit nur Leute ihn sehen können, die du kennst.

{{< subHeadline >}}Nutze Multifaktor-Authentifizierung{{< /subHeadline >}}

Sicher hast du schon mal etwas von „2FA“ gehört. Das steht für „two-factor authentication“, also Zwei-Faktor-Authentifizierung, und bedeutet, dass man mehr für das Einloggen in einen Account benötigt als nur ein Passwort, also z.B. einen sog. OTP-Code, deinen Fingerabdruck oder einen Sicherheits-USB-Stick. Aktiviere 2FA überall, wo es möglich ist, um zu verhindern, dass andere auf deinen Account zugreifen können, nur indem sie dein Passwort erraten.

Bei Mobilsicher erfährst du {{< link "https://mobilsicher.de/ratgeber/zwei-faktor-authentisierung">}}mehr zum Thema{{< /link >}}.

{{< subHeadline >}}Verwende einen Passwort-Manager{{< /subHeadline >}}

Benutze nicht das gleiche Passwort auf jeder Website, wähle keine einfach erratbaren Passwörter, schreibe sie nicht auf. Aber wie merkt man sich dann Dutzende komplizierter Zeichenketten? Nun, man tut es nicht. Nutze stattdessen einen Passwort-Manager – also einen digitalen Tresor für all deine verschiedenen Passwörter.

PhishWarn empfiehlt {{< link "https://keepassxc.org">}}KeePassXC{{< /link >}}.

{{< subHeadline >}}Informiere dich regelmäßig in einschlägigen Medien{{< /subHeadline >}}

Um auf dem Laufenden zu bleiben, welche neuen Tricks sich die Betrüger ausgedacht haben und wie man sich schützen kann, solltest du dich regelmäßig in einschlägigen Medien über die Thematik informieren.

{{< details >}}
Zu empfehlen sind beispielsweise
die Technik-Magazine
<br>
<br>- {{< link "https://heise.de">}}Heise{{< /link >}} und {{< link "https://www.heise.de/ct/">}}c't{{< /link >}},
<br>- {{< link "https://golem.de">}}Golem{{< /link >}},
<br>- {{< link "https://t3n.de">}}t3n{{< /link >}},
<br>- {{< link "https://www.zdnet.de">}}ZDNet{{< /link >}} und
<br>- {{< link "https://arstechnica.com">}}Ars Technica{{< /link >}},
<br><br>
aber auch kleinere Blogs wie {{< link "https://mobilsicher.de">}}Mobilsicher{{< /link >}} oder der {{< link "https://kuketz-blog.de">}}Kuketz-Blog{{< /link >}} können immer wieder sehr aufschlussreich sein. Auch staatliche Stellen haben eigene Informationsangebote, z.B.:
<br>
<br>- das IT-Sicherheits-Bundesamt {{< link "https://www.bsi.bund.de">}}BSI{{< /link >}},
<br>- „{{< link "https://www.sicher-im-netz.de">}}Deutschland sicher im Netz{{< /link >}}“ vom Innenministerium,
<br>- die {{< link "https://verbraucherzentrale.de">}}Verbraucherschutzzentrale(n){{< /link >}},
<br>- das englische {{< link "https://www.ncsc.gov.uk">}}National Cyber Security Centre{{< /link >}} oder
<br>- das Sicherheitsportal des guten alten {{< link "https://www.fbi.gov/scams-and-safety">}}FBI{{< /link >}}.
{{< /details >}}
{{< /article >}}

{{< teaser "recognizePhishing" "de" >}}
